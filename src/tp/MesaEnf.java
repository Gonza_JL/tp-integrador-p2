package tp;

public class MesaEnf extends Mesa {

	public MesaEnf(Integer numero, Persona presidente) {
		super(numero, presidente);
		franjasParaTurnos(8, 18);
		franjasParaCupos(8, 18, 20);
	}

}
