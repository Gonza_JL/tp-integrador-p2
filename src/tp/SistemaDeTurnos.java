package tp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SistemaDeTurnos {
	
	/* IREP:
	 * - Para toda clave c de mesas, c == Mesa.id
	 * - Para toda clave c de votantes, c == Persona.dni
	 */
	
	private String nombreSistema;
	private Map<Integer, Mesa> mesas; // idMesa -> Mesa
	private Map<Integer, Persona> votantes; // DNI -> Persona
	private Map<Integer, Turno> turnosAsignados; // DNI -> Turno
	
	/* Constructor del sistema de asignacion de turnos. 
	 * Si el parametro nombre fuera null, debe generar una excepcion.
	 */
	public SistemaDeTurnos(String nombreSistema) {
		if(nombreSistema == null || nombreSistema.isEmpty())
			throw new RuntimeException("Nombre de sistema invalido");
		this.nombreSistema = nombreSistema;
		mesas = new HashMap<>();
		votantes = new HashMap<>();
		turnosAsignados = new HashMap<>();
	}

	/* Registrar a los votantes. Antes de asignar un turno el votante debe estar registrado 
	 * en el sistema. 
	 * Si no lo esta, se genera una excepcion.
	 * Si la edad es menor a 16, se genera una excepcion
	 */
	public void registrarVotante(int dni, String nombre, int edad, boolean enfPrevia, boolean trabaja) {
		if(edad < 16) {
			throw new RuntimeException("Votante invalido: debe tener una edad mayor o igual a 16 años");
		}
		Persona votante = new Persona(dni, nombre, edad, enfPrevia, trabaja);
		votantes.put(dni, votante);
	}

	/* Agregar una nueva mesa del tipo dado en el parametro y asignar el presidente 
	 *  de cada una, el cual deberia estar entre los votantes registrados y sin turno asignado.
	 *  -  Devuelve el numero de mesa  creada.
	 *  si el presidente es un votante que no esta registrado debe devolver Empty
	 *  si el tipo de mesa no es valido debe generar una excepcion
	 *  Los tipos validos son:  Enf_Preex, Mayor65, General y Trabajador
	 */
	public int agregarMesa(final String tipoMesa, int dni) {
		if(!votantes.containsKey(dni)) {
			throw new RuntimeException("Presidente no registrado en votantes");
		}
		int idMesa = mesas.size() + 1;
		if(tipoMesa.equals("Enf_Preex")) {
			mesas.put(idMesa, new MesaEnf(idMesa, votantes.get(dni)));
		} else if(tipoMesa.equals("Mayor65")) {
			mesas.put(idMesa, new MesaMayores(idMesa, votantes.get(dni)));
		} else if(tipoMesa.equals("General")) {
			mesas.put(idMesa, new MesaSimple(idMesa, votantes.get(dni)));
		} else if(tipoMesa.equals("Trabajador")) {
			mesas.put(idMesa, new MesaTrabajadores(idMesa, votantes.get(dni)));
		} else { 
			throw new RuntimeException("Tipo de mesa invalida");
		}
		asignarYGuardarTurno(dni, mesas.get(idMesa));
		return idMesa;
	}
	
	private Tupla<Integer, Integer> asignarYGuardarTurno(int dni, Mesa mesa) {
		Turno turno = mesa.asignarTurno(dni);
		if(turno != null) {
			turnosAsignados.put(dni, turno); 
			return new Tupla<Integer, Integer>(turno.idMesa(), turno.horario());
		}
		return null;
	}

	/* Asigna un turno a un votante determinado. 
	 *   -  Si el dni no pertenece a un votante registrado debe generar una excepcion.
	 *   -  Si el votante ya tiene turno asignado se devuelve el turno como: Numero de 
	 *        Mesa y Franja Horaria.
	 *  -  Si aun no tiene turno asignado se busca una franja horaria disponible en una
	 *       mesa del tipo correspondiente al votante  y se devuelve el turno asignado, como
	 *       Numero de Mesa y Franja Horaria.
	 *  -  Si no hay mesas con horarios disponibles no modifica nada y devuelve null.
	 *  (Se supone que el turno permitira conocer la mesa y la franja horaria asignada)
	 */
	public Tupla<Integer, Integer> asignarTurno(int dni) {
		if(!votantes.containsKey(dni)) {
			throw new RuntimeException("Votante no registrado");
		}
		Persona votante = votantes.get(dni);
		for(Mesa mesa: mesas.values()) {
			if(votante.trabaja()) {
				if(mesa instanceof MesaTrabajadores) {
					return asignarYGuardarTurno(dni, mesa);
				}
			} else {
				if(votante.esMayorDe65() && mesa instanceof MesaMayores) {
					return asignarYGuardarTurno(dni, mesa);
				} else if(votante.tieneEnfermedad() && mesa instanceof MesaEnf) {
					return asignarYGuardarTurno(dni, mesa);
				} else if(votante.esSimple() && mesa instanceof MesaSimple) {
					return asignarYGuardarTurno(dni, mesa);
				}
			}
		}
		return null;
	}

	/* Asigna turnos automoticamente a los votantes sin turno. 
	 *  El sistema busca si hay alguna mesa y franja horaria factible en la que haya disponibilidad.
	 *  Devuelve la cantidad de turnos que pudo asignar.
	 */
	public int asignarTurnos() {
		int cont = 0;
		for(Integer dni: votantes.keySet()) {
				if(consultaTurno(dni) == null) {
					asignarTurno(dni);
					cont++;
				}
		}
		return cont;
	}

	/* Hace efectivo el voto del votante determinado por su dni.
	 *  Si el DNI no esta registrado entre los votantes debe generar una excepcion
	 *  Si ya habia votado devuelve false.
	 *  Sino, efectua el voto y devuelve true.
	 */
	public boolean votar(int dni) {
		if(!votantes.containsKey(dni)) {
			throw new RuntimeException("Votante no registrado");
		}
		if(votantes.get(dni).voto()) {
			return false;
		}
		votantes.get(dni).setVoto(true);
		return true;
	}
	
	/* 
	 * Cantidad de votantes con Turno asignados al tipo de mesa que se pide.
	 * -  Permite conocer cuantos votantes se asignaron hasta el momento a alguno
	 *      de los tipos de mesa que componen el sistema de votacion.
	 * -  Si la clase de mesa solicitada no es valida debe generar una excepcion
	 */
	public int votantesConTurno(final String tipoMesa) {
		for(Mesa mesa: mesas.values()) {
			if(tipoMesa.equals("Mayor65") && mesa instanceof MesaMayores) {
				return mesa.votantesConTurno();
			} else if(tipoMesa.equals("Enf_Preex") && mesa instanceof MesaEnf) {
				return mesa.votantesConTurno();
			} else if(tipoMesa.equals("Trabajador") && mesa instanceof MesaTrabajadores) {
				return mesa.votantesConTurno();
			} else if(tipoMesa.equals("General") && mesa instanceof MesaSimple) {
				return mesa.votantesConTurno();
			} else {
				throw new RuntimeException("Tipo de mesa invalida");
			}
		}
		return 0;
	}

	/* Consulta el turno de un votante dado su DNI. Devuelve Mesa y franja horaria.
	* -  Si el DNI no pertenece a un votante genera una excepcion.
	* -  Si el votante no tiene turno devuelve Empty.
	*/
	public Tupla<Integer, Integer> consultaTurno(int dni) {
		if(!votantes.containsKey(dni)) {
			throw new RuntimeException("Votante no registrado");
		}
		if(turnosAsignados.get(dni) != null) {
			return new Tupla<Integer, Integer>(turnosAsignados.get(dni).idMesa(), turnosAsignados.get(dni).horario());
		}
		return null;
	}

	/* Dado un numero de mesa, devuelve una Map cuya clave es la franja horaria y
	 *  el valor es una lista con los DNI de los votantes asignados a esa franja.  
	 *  Sin importar si se presentaron o no a votar.
	 *  -  Si el numero de mesa no es valido genera una excepcion.
	 *  -  Si no hay asignados devuelve un Map vacio.
	 */
	public Map<Integer, List<Integer>> asignadosAMesa(int numMesa) {
		if(!mesas.containsKey(numMesa)) {
			throw new RuntimeException("Numero de mesa invalido");
		}
		return mesas.get(numMesa).asignadosAMesa();
	}

	/*
	 *  Consultar la cantidad de votantes sin turno asignados a cada tipo de mesa.
	 *  Devuelve una Lista de Tuplas donde se vincula el tipo de mesa con la cantidad
	 *  de votantes sin turno que esperan ser asignados a ese tipo de mesa.
	 *  La lista no puede tener 2 elementos para el mismo tipo de mesa.
	 */
	public List<Tupla<String, Integer>> sinTurnoSegunTipoMesa() {
		int[] contadores = {0, 0, 0, 0};
		List<Tupla<String, Integer>> votantesSinTurno = new ArrayList<>();
		for(Integer dni: votantes.keySet()) {
			Persona votante = votantes.get(dni);
			if(consultaTurno(dni) == null) {
				if(votante.esMayorDe65()) {
					contadores[0]++;
				} else if(votante.tieneEnfermedad()) {
					contadores[1]++;	
				} else if(votante.trabaja()) {
					contadores[2]++;	
				} else if(votante.esSimple()) {
					contadores[3]++;
				}
			}	
		}
		List<String> tiposDeMesa = List.of("Mayor65", "Enf_Preex", "Trabajador", "General");
		for(int i = 0; i < 4; i++) {
			Tupla<String, Integer> tipoMesaCant = new Tupla<>(tiposDeMesa.get(i), contadores[i]);
			votantesSinTurno.add(tipoMesaCant);
		}
		return votantesSinTurno;
	}
	
	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("Sistema de Turnos para Votación - ");
		st.append(nombreSistema);
		st.append("\nMesas:\n");
		for(Mesa mesa: mesas.values()) {
			st.append(mesa.toString());
			st.append("\n");
		}
		st.append("Votantes:\n");
		for(Persona votante: votantes.values()) {
			st.append(votante.toString());
			st.append("\n");
		}
		return st.toString();
	}
	
}
