package tp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public abstract class Mesa {
	
	/* IREP:
	 * - Para toda clave c de horarioTurnos, c == a los horarios de los turnos del value
	 */
	
	private final Integer id;
	private Persona presidente;
	private Map<Integer, HashSet<Turno>> horarioTurnos;  // horario de turnos -> ConjuntoDeTurnos
	private Map<Integer, Integer> horarioCupo;  // horario de turnos -> Cupo
	
	public Mesa(Integer id, Persona presidente) {
		this.id = id;
		this.presidente = presidente;
		this.horarioTurnos = new HashMap<>();
		this.horarioCupo = new HashMap<>();
	}
	
	protected void franjasParaTurnos(int desdeLasHs, int hastaLasHs) {
		for(int horario = desdeLasHs; horario < hastaLasHs; horario++) {
			horarioTurnos.put(horario, new HashSet<>());
		}
	}
	
	protected void franjasParaCupos(int desdeLasHs, int hastaLasHs, int cupo) {
		for(int horario = desdeLasHs; horario < hastaLasHs; horario++) {
			horarioCupo.put(horario, cupo);
		}
	}
	
	public Turno asignarTurno(int dni) {
		final Integer horarioDisp = horarioDisponible();
		if(horarioDisp == null) {
			return null;
		}
		final Turno turno = new Turno(id, dni, horarioDisp);
		if(horarioTurnos.get(horarioDisp).contains(turno)) {
			return turno;
		} else {
			horarioTurnos.get(horarioDisp).add(turno);
			horarioCupo.replace(horarioDisp, horarioCupo.get(horarioDisp) - 1);
			return turno;
		}
	}
	
	public Integer horarioDisponible() {
		for(Integer horario: horarioCupo.keySet()) {
			if(horarioCupo.get(horario) > 0) {
				return horario;
			}
		}
		return null;
	}

	public int votantesConTurno() {
		int suma = 0;
		for(HashSet<Turno> turnos: horarioTurnos.values()) {
			suma = suma + turnos.size();
		}
		return suma;
	}

	public Map<Integer, List<Integer>> asignadosAMesa() {
		Map<Integer, List<Integer>> asignados = new HashMap<>();
		for(Integer horario: horarioTurnos.keySet()) {
			List<Integer> lstDNI = new ArrayList<>();
			for(Turno turno: horarioTurnos.get(horario)) {
				lstDNI.add(turno.dniPersona());
			}
			asignados.put(horario, lstDNI);
		}
		return asignados;
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("ID: ");
		st.append(id);
		st.append(", tipo de mesa: ");
		st.append(this.getClass().getSimpleName());
		st.append(", presidente: ");
		st.append(presidente.dni());
		st.append(", turnos: ");
		for(Integer horario: horarioTurnos.keySet()) {
			st.append(horarioTurnos.get(horario));
			st.append("; ");
		}
		return st.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!this.getClass().getName().equals(obj.getClass().getName())) {
			return false;
		}
		Mesa otraMesa = (Mesa)obj;
		if(!this.id.equals(otraMesa.id)) {
			return false;
		}
		for(Integer horario: horarioTurnos.keySet()) {
			if(!horarioTurnos.get(horario).equals(otraMesa.horarioTurnos.get(horario)))
				return false;
		}
		return true;
	}

}
