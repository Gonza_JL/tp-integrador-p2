package tp;

public class Persona {
	
	private final Integer dni;
	private String nombre;
	private Integer edad;
	private boolean tieneEnfermedad;
	private boolean trabaja;
	private boolean voto;
	
	public Persona(Integer dni, String nombre, Integer edad, boolean tieneEnfermedad, boolean trabaja) {
		this.dni = dni;
		this.nombre = nombre;
		this.edad = edad;
		this.tieneEnfermedad = tieneEnfermedad;
		this.trabaja = trabaja;
		voto = false;
	}

	public void setVoto(boolean voto) {
		this.voto = voto;
	}

	public boolean esSimple() {
		return !esMayorDe65() && !tieneEnfermedad() && !trabaja();
	}

	public boolean esMayorDe65() {
		return edad > 65;
	}

	public boolean tieneEnfermedad() {
		return tieneEnfermedad;
	}

	public boolean trabaja() {
		return trabaja;
	}

	public boolean voto() {
		return voto;
	}

	public Integer dni() {
		return dni;
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("DNI: ");
		st.append(dni);
		st.append(", nombre: ");
		st.append(nombre);
		st.append(", edad: ");
		st.append(edad);
		if(voto) {
			st.append(", la persona votó.");
		} else {
			st.append(", la persona no votó.");
		}
		return st.toString();
	}
	
	

}
