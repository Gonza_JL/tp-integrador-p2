package tp;

public class MesaSimple extends Mesa {

	public MesaSimple(Integer numero, Persona presidente) {
		super(numero, presidente);
		franjasParaTurnos(8, 18);
		franjasParaCupos(8, 18, 30);
	}

}
