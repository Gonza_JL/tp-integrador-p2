package tp;

public class MesaMayores extends Mesa {

	public MesaMayores(Integer numero, Persona presidente) {
		super(numero, presidente);
		franjasParaTurnos(8, 18);
		franjasParaCupos(8, 18, 10);
	}

}
