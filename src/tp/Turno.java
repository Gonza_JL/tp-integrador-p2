package tp;

public class Turno {
	
	/* IREP:
	 * - 8 <= horario < 18
	 */
	
	private final Integer idMesa;
	private final Integer dniPersona;
	private final Integer horario;
	
	public Turno(Integer idMesa, Integer dniPersona, Integer horario) {
		this.idMesa = idMesa;
		this.dniPersona = dniPersona;
		this.horario = horario;
	}

	public Integer dniPersona() {
		return dniPersona;
	}

	public Integer horario() {
		return horario;
	}
	
	public Integer idMesa() {
		return idMesa;
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("ID de mesa: ");
		st.append(idMesa);
		st.append(", DNI: ");
		st.append(dniPersona);
		st.append(", horario: ");
		st.append(horario);
		return st.toString();
	}
	
	
	
	
	
	
	

}
