package tp;

public class MesaTrabajadores extends Mesa {

	public MesaTrabajadores(Integer numero, Persona presidente) {
		super(numero, presidente);
		franjasParaTurnos(8, 12);
		franjasParaCupos(8, 12, Integer.MAX_VALUE);
	}

}
